package pl.edu.pwr.gui;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

//import com.thaiopensource.util.Equal;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
/**
 * Created by nieop on 09.06.2016.
 */
public class ImageConverterTest {

    private BufferedImage image = new BufferedImage(50, 100, BufferedImage.TYPE_BYTE_GRAY);

    //Testy na zwracanie odpowiedniego rozmiaru

    @Test
    public void shouldReturnTrueAnd80x160() {
        assertThat(ImageConverter.getNewWidthAndHeight(size.width80, image), is(true));
        assertThat(ImageConverter.getNewWidth(), is(80));
        assertThat(ImageConverter.getNewHeight(), is(160));
    }

    @Test
    public void shouldReturnTrueAnd160x320() {
        assertThat(ImageConverter.getNewWidthAndHeight(size.width160, image), is(true));
        assertThat(ImageConverter.getNewWidth(), is(160));
        assertThat(ImageConverter.getNewHeight(), is(320));
    }

    @Test
    public void shouldReturnTrueAnd50x100() {
        assertThat(ImageConverter.getNewWidthAndHeight(size.orginalSize, image), is(true));
        assertThat(ImageConverter.getNewWidth(), is(50));
        assertThat(ImageConverter.getNewHeight(), is(100));
    }

    @Test
    public void shouldReturnFalseNoImage() {
        assertThat(ImageConverter.getNewWidthAndHeight(size.orginalSize, null), is(false));
    }

    @Test
    public void shouldReturnFalseNoChoice() {
        assertThat(ImageConverter.getNewWidthAndHeight(null, image), is(false));
    }

    @Test
    public void shouldReturnRightSizeOfIntensitiesArray() {
        int[][] _intensities = ImageConverter.fillIntensitiesArray(image);
        assertThat(_intensities.length, is(100));
        assertThat(_intensities[0].length, is(50));
    }

    @Test
    public void shouldReturnNullIfNoImage() {
        assertThat(ImageConverter.fillIntensitiesArray(null), is(equalTo(null)));
    }
}
