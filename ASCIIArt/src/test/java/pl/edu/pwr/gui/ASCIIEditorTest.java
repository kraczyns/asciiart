package pl.edu.pwr.gui;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

//import com.thaiopensource.util.Equal;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
/**
 * Created by Użytkownik on 2016-06-09.
 */
public class ASCIIEditorTest {

    ASCIIEditor asciiEditor = new ASCIIEditor();
 //   asciiEditor.OKButtonActionListener
    ASCIIEditor.OKButtonActionListener okButtonActionListener;

    @Test
    public void shouldReturnWidth80()
    {
        asciiEditor.eightySignsRadioButton.setSelected(true);
        assertThat(asciiEditor.sizeSelection(), is(size.width80));
    }

    @Test
    public void shouldReturnWidth160()
    {
        asciiEditor.hundredSixtySignsRadioButton.setSelected(true);
        assertThat(asciiEditor.sizeSelection(), is(size.width160));
    }

    @Test
    public void shouldReturnOriginalSize()
    {
        asciiEditor.orginalSizeRadioButton.setSelected(true);
        assertThat(asciiEditor.sizeSelection(), is(size.orginalSize));
    }

    @Test
    public void shouldReturnScreenSize()
    {
        asciiEditor.sizeOfScreenRadioButton.setSelected(true);
        assertThat(asciiEditor.sizeSelection(), is(size.screenSize));
    }

    @Test
    public void shouldThrowException()
    {
        String urlAdress = "http://nonexistingadress.com";
        try {
            okButtonActionListener.convertFromPGMToBufferedImage(urlAdress);
        }
        catch (Exception e)
        {
            assertThat(e, is(instanceOf(NullPointerException.class)));
        }
    }

}
