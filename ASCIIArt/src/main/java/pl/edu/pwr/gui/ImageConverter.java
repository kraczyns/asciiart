package pl.edu.pwr.gui;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

enum size
{
	width80,
	width160,
	screenSize,
	orginalSize
}

public class ImageConverter {

	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	private static String INTENSITY_2_ASCII = "@%#*+=-:. ";
	private static String INTENSITY_2_ASCII_70 = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";
	private static boolean highQuality = false;
	private static boolean lowQuality = false;
	private static int newWidth = 0;
	private static int newHeight = 0;
	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */

	public static int getNewWidth()
	{
		return newWidth;
	}

	public static int getNewHeight()
	{
		return newHeight;
	}

	public static void setQuality(boolean _highQuality, boolean _lowQuality)
	{
	highQuality = _highQuality;
	lowQuality = _lowQuality;
	}

	public static char intensityToAsciiLow(int intensity)
	{
		double step = (double) 256 / INTENSITY_2_ASCII.length();
		int index =  (int) (intensity / step);
		return INTENSITY_2_ASCII.charAt(index);
	}

	public static char intensityToAsciiHigh(int intensity)
	{
		double step = (double) 256 / INTENSITY_2_ASCII_70.length();
		int index = (int) (intensity / step);
		return INTENSITY_2_ASCII_70.charAt(index);
	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities) {
		if (!highQuality && !lowQuality)
			return null;
		int numberOfRows = intensities.length;
		int numberOfCols = intensities[0].length;
		char[][] Ascii = new char[numberOfRows][numberOfCols];

		for (int i = 0; i < numberOfRows; i++)
		{
			for (int j = 0; j < numberOfCols; j++)
			{
				if (highQuality)
					Ascii[i][j] = intensityToAsciiHigh(intensities[i][j]);
				else if(lowQuality)
					Ascii[i][j] = intensityToAsciiLow(intensities[i][j]);
			}
		}
		return Ascii;
	}

	public static boolean getNewWidthAndHeight(size choice, BufferedImage image)
	{
		if (choice == null || image == null)
			return false;

		switch (choice)
		{
			case width80:
			{
				newWidth = 80;
				break;
			}
			case width160:
			{
				newWidth = 160;
				break;
			}
			case screenSize:
			{
				Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
				newWidth = (int)screen.getWidth();
				break;
			}
			case orginalSize:
			{
				newWidth = image.getWidth();
				break;
			}
		}
			newHeight = image.getHeight() * newWidth / image.getWidth();

		return true;
	}

	public static BufferedImage getResizedBufferedImage(BufferedImage image)
	{
		BufferedImage resizedImage;

		Image tmp = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
		resizedImage= new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2d = resizedImage.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();

		return resizedImage;
	}

	public static int[][] fillIntensitiesArray(BufferedImage image)
	{
		if (image == null)
			return null;

		int[][] intensities = new int[image.getHeight()][];
		for (int i = 0; i < image.getHeight(); i++) {
			intensities[i] = new int[image.getWidth()];
		}
		Raster raster = image.getRaster();
		for (int x = 0; x < image.getWidth(); x++) {
			for (int y = 0; y < image.getHeight(); y++) {
				intensities[y][x] = raster.getSample(x, y, 0);
			}
		}
		return intensities;
	}

	public static BufferedImage convertFromColorToGrayScale(BufferedImage image)
	{
		BufferedImage convertedImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster raster = convertedImage.getRaster();

		for(int y = 0; y < image.getHeight(); y++)
			for(int x = 0; x < image.getWidth(); x++)
			{
				Color color = new Color(image.getRGB(x, y));
				raster.setSample(x, y, 0, (int)((0.2989 * color.getRed()) + (0.5870 * color.getGreen()) + (0.1140 * color.getBlue())));
			}
		return convertedImage;
	}


}
