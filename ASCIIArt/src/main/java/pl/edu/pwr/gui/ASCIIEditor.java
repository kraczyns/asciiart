package pl.edu.pwr.gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Użytkownik on 2016-05-17.
 */
public class ASCIIEditor extends JFrame {

    private JPanel rootPanel;
    private JButton wczytajObrazButton;
    private JButton zapiszDoPlikuButton;
    private JButton funkcja1Button;
    private JButton funkcja2Button;
    protected JLabel imageLabel;
    protected JPanel imagePanel;
    private JRadioButton lowQualityRadioButton;
    private JRadioButton wysokaJakoscRadioButton;
    protected JRadioButton eightySignsRadioButton;
    protected JRadioButton hundredSixtySignsRadioButton;
    protected JRadioButton sizeOfScreenRadioButton;
    protected JRadioButton orginalSizeRadioButton;
    private JRadioButton highQualityRadioButton;
    private DialogForm dialogform;
    private BufferedImage image;
    private Path imagePath;
    private URL url;
    private String imagePathString;
    private boolean isPGM;
    private boolean isInGrayScale;

    public class OKButtonActionListener implements ActionListener {

        public BufferedImage convertFromPGMToBufferedImage(String _from) {
            try {
                ImageFileReader imageFileReader = new ImageFileReader();
                int[][] intensities = imageFileReader.readPgmFile(imagePathString, _from);
                int height = intensities.length;
                int width = intensities[0].length;
                image = new BufferedImage(width, height,
                        BufferedImage.TYPE_BYTE_GRAY);
                WritableRaster raster = image.getRaster();
                for (int y = 0; y < height; y++) {
                    for (int x = 0; (x < width); x++) {
                        raster.setSample(x, y, 0, intensities[y][x]);
                    }
                }
            } catch (URISyntaxException e1) {
                e1.printStackTrace();
                return null;
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
                JOptionPane.showMessageDialog(null, "Podano zły adres URL");
                return null;
            }
            return image;
        }

        public void showImageInBox(ImageIcon _imageIcon) {
            imageLabel.setText(null);
            imageLabel.setIcon(_imageIcon);
            dialogform.dispose();
        }


        @Override
        public void actionPerformed(ActionEvent e) {
            //nieważne jaki obrazek zapisz do pliku ma być aktywny
            zapiszDoPlikuButton.setEnabled(true);
            if (dialogform.zAdresuURLRadioButton.isSelected() && dialogform.URLTextField.getText().endsWith(".pgm")) {
                isPGM = true;
                    imagePathString = dialogform.URLTextField.getText();
                    if ((image = convertFromPGMToBufferedImage("fromURL"))!=null)
                        showImageInBox(new ImageIcon(image));
            } else if (dialogform.zDyskuRadioButton.isSelected() && dialogform.PathLabel.getText().endsWith(".pgm")) {
                isPGM = true;
                    imagePathString = dialogform.PathLabel.getText();
                if ((image = convertFromPGMToBufferedImage("fromFile"))!=null)
                    showImageInBox(new ImageIcon(image));
            } else if (!(dialogform.zDyskuRadioButton.isSelected() || dialogform.zAdresuURLRadioButton.isSelected())) {
                JOptionPane.showMessageDialog(null, "Nie wybrano żadnej opcji");
            } else if (dialogform.zAdresuURLRadioButton.isSelected() && !(dialogform.URLTextField.getText().endsWith(".pgm"))) {
                    try {
                        url = new URL(dialogform.URLTextField.getText());
                        ImageIcon imageIcon = new ImageIcon(url);
                        image = ImageIO.read(url);
                        showImageInBox(imageIcon);
                    } catch (MalformedURLException e1) {
                        //e1.printStackTrace();
                        JOptionPane.showMessageDialog(null,"Podano zły adres URL");
                    } catch (java.io.IOException ex) {
                        JOptionPane.showMessageDialog(null,"Podano zły adres URL");
                    }
            } else if (dialogform.zDyskuRadioButton.isSelected() && !(dialogform.PathLabel.getText().endsWith(".pgm"))) {
                ImageIcon imageIcon = new ImageIcon(dialogform.PathLabel.getText());
                imagePath = Paths.get(dialogform.PathLabel.getText());
                try {
                    image = ImageIO.read(imagePath.toFile());
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(null,"Nie udało się przetworzyć pliku");
                }
                showImageInBox(imageIcon);
            }

            if (image.getType() == BufferedImage.TYPE_BYTE_GRAY)
                isInGrayScale = true;
        }
    }

    public size sizeSelection()
    {
        size choice = null;

        if (eightySignsRadioButton.isSelected())
            choice = size.width80;
        else if (hundredSixtySignsRadioButton.isSelected())
            choice = size.width160;
        else if (orginalSizeRadioButton.isSelected())
            choice = size.orginalSize;
        else if (sizeOfScreenRadioButton.isSelected())
            choice = size.screenSize;

        return choice;
    }

    public void qualitySelection()
    {
        ImageConverter.setQuality(false, false);
        if (lowQualityRadioButton.isSelected())
            ImageConverter.setQuality(false, true);
        if (highQualityRadioButton.isSelected())
            ImageConverter.setQuality(true, false);
    }

    public void groupButtons()
    {
        ButtonGroup qualitySelectionGroup = new ButtonGroup();
        qualitySelectionGroup.add(highQualityRadioButton);
        qualitySelectionGroup.add(lowQualityRadioButton);

        ButtonGroup sizeSelectionGroup = new ButtonGroup();
        sizeSelectionGroup.add(eightySignsRadioButton);
        sizeSelectionGroup.add(hundredSixtySignsRadioButton);
        sizeSelectionGroup.add(sizeOfScreenRadioButton);
        sizeSelectionGroup.add(orginalSizeRadioButton);
    }

        public ASCIIEditor() {

            setContentPane(rootPanel);
            pack();
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            zapiszDoPlikuButton.setEnabled(false);
            groupButtons();
            funkcja1Button.setEnabled(false);
            funkcja2Button.setEnabled(false);
            isPGM = isInGrayScale = false;

            wczytajObrazButton.addActionListener(e ->
            {
                    dialogform = new DialogForm(ASCIIEditor.this);
            });

            zapiszDoPlikuButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    qualitySelection();
                    size choice = sizeSelection();

                    JFileChooser fc = new JFileChooser();
                    ImageFileWriter imageFileWriter = new ImageFileWriter();
                    char[][] ascii;
                    File file;
                    FileNameExtensionFilter filter = new FileNameExtensionFilter("Text File", "txt");
                    fc.setFileFilter(filter);

                    if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                        ImageConverter.getNewWidthAndHeight(choice, image);
                        image = ImageConverter.getResizedBufferedImage(image);

                        if (!isPGM && !isInGrayScale)
                                image = ImageConverter.convertFromColorToGrayScale(image);

                        ascii = ImageConverter.intensitiesToAscii(ImageConverter.fillIntensitiesArray(image));
                        if (ascii == null)
                            JOptionPane.showMessageDialog(null,"Nie wybrano jakości obrazka");
                        else {
                            file = fc.getSelectedFile();
                            try {
                                imageFileWriter.saveToTxtFile(ascii, file.getPath() + ".txt");
                            } catch (FileNotFoundException e1) {
                                JOptionPane.showMessageDialog(null,"Nie znaleziono pliku");
                            }
                        }

                    }
                     //WYŚWIETLENIE ZKONWERTOWANEGO OBRAZU
                   /* ImageIcon imageIcon = new ImageIcon(image);
                    imageLabel.setText(null);
                    imageLabel.setIcon(imageIcon);
                    dialogform.dispose(); */
                }
            });

            setVisible(true);
        }

    }