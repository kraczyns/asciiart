package pl.edu.pwr.gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageFileReader {

	/**
	 * Metoda czyta plik pgm i zwraca tablicę odcieni szarości.
	 * @param fileName nazwa pliku pgm
	 * @return tablica odcieni szarości odczytanych z pliku
	 * @throws URISyntaxException jeżeli plik nie istnieje
	 */


	private BufferedReader createBufferedReader(String method, String fileName) throws IOException, URISyntaxException {
		if (method.equals("fromURL")) {
			URL url = new URL(fileName);
			return new BufferedReader(new InputStreamReader(url.openStream()));
		} else if(method=="fromFile") {
			Path path = Paths.get(fileName);
			return  Files.newBufferedReader(path);
		}
		else if(method=="test")
		{
			Path path = getPathToFile(fileName);
			return Files.newBufferedReader(path);
		}
		return null;
	}

	public int[][] readPgmFile(String fileName,String method) throws URISyntaxException, MalformedURLException {
		int columns;
		int rows;
		int[][] intensities = null;
		try (BufferedReader reader = createBufferedReader(method, fileName)) {

			reader.readLine();
			reader.readLine();

			String[] cols_rows = reader.readLine().split(" ");
			columns = Integer.parseInt(cols_rows[0]);
			rows = Integer.parseInt(cols_rows[1]);

			int maxValue = Integer.parseInt(reader.readLine());

			intensities = new int[rows][];

			for (int i = 0; i < rows; i++) {
				intensities[i] = new int[columns];
			}

			String line;
			int currentRow = 0;
			int currentColumn = 0;
			while ((line = reader.readLine()) != null) {
				String[] elements = line.split(" ");
				for (int i = 0; i < elements.length; i++) {
					intensities[currentRow][currentColumn] = Integer.parseInt(elements[i]);

					currentColumn++;
					if (currentColumn == columns) {
						currentColumn = 0;
						currentRow++;
						if (currentRow == rows)
							break;
					}
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return intensities;
	}
	
	private Path getPathToFile(String fileName) throws URISyntaxException {
		URI uri = ClassLoader.getSystemResource(fileName).toURI();
		return Paths.get(uri);
	}
	
}
