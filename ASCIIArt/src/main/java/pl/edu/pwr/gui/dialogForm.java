package pl.edu.pwr.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

/**
 * Created by Użytkownik on 2016-05-17.
 */
public class DialogForm extends JFrame {

    private final ASCIIEditor parent;
    private JPanel dialogRootPanel;
    protected JRadioButton zDyskuRadioButton;
    private JButton wybierzPlikButton;
    protected JRadioButton zAdresuURLRadioButton;
    protected JButton OKButton;
    protected JButton anulujButton;
    protected JTextField URLTextField;
    protected JLabel PathLabel;
    protected String fileName;
    String filePath;

    public DialogForm(ASCIIEditor parent)
    {
        this.parent = parent;
        setContentPane(dialogRootPanel);
        pack();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        URLTextField.setEditable(false);
        wybierzPlikButton.setEnabled(false);

        anulujButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               dispose();
            }
        });

        OKButton.addActionListener(parent.new OKButtonActionListener());

        zDyskuRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(zAdresuURLRadioButton.isSelected())
                {
                    zAdresuURLRadioButton.setSelected(false);
                    URLTextField.setText("Adres URL...");
                    URLTextField.setEditable(false);
                }

                wybierzPlikButton.setEnabled(true);
            }
        });

        zAdresuURLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(zDyskuRadioButton.isSelected())
                {
                    zDyskuRadioButton.setSelected(false);
                    wybierzPlikButton.setEnabled(false);
                    PathLabel.setText("Scieżka do pliku");
                    pack();

                }
                URLTextField.setText(null);
                URLTextField.setEditable(true);
            }
        });

        wybierzPlikButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();

                if(fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
                {
                    File file = fc.getSelectedFile();
                    filePath = file.getPath();
                    fileName = file.getName();
                    PathLabel.setText(filePath);
                }
                pack();
            }
        });

        setVisible(true);
    }
}
