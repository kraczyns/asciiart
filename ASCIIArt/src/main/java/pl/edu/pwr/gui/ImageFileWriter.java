package pl.edu.pwr.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) throws FileNotFoundException {

		File file = new File(fileName);
		PrintWriter printWriter = new PrintWriter(file);

			for (int i = 0; i < ascii.length; i++) {
				for (int j = 0; j < ascii[i].length; j++) {
					printWriter.print(ascii[i][j]);
				}
				printWriter.println();
			}
		printWriter.close();
	}
}
